import React from 'react';
import { View, StyleSheet } from 'react-native';
import { FontAwesome, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { red, orange, blue, lightPurp, pink, white } from './colors';

export function isBetween (num, x, y) {
	/* Function returns True if num is in [x, y] or in [y, x]*/
  if (num >= x && num <= y) {
    return true
  }
  return false
}

const styles = StyleSheet.create({
  iconContainer: {
    padding: 5,
    borderRadius: 8,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20
  },
});


export function calculateDirection (heading) {
  let direction = '';

  if (isBetween(heading, 0, 22.5)) {
    direction = 'North'
  } else if (isBetween(heading, 22.5, 67.5)) {
    direction = 'North East'
  } else if (isBetween(heading, 67.5, 112.5)) {
    direction = 'East'
  } else if (isBetween(heading, 112.5, 157.5)) {
    direction = 'South East'
  } else if (isBetween(heading, 157.5, 202.5)) {
    direction = 'South'
  } else if (isBetween(heading, 202.5, 247.5)) {
    direction = 'South West'
  } else if (isBetween(heading, 247.5, 292.5)) {
    direction = 'West'
  } else if (isBetween(heading, 292.5, 337.5)) {
    direction = 'North West'
  } else if (isBetween(heading, 337.5, 360)) {
    direction = 'North'
  } else {
    direction = 'Calculating'
  }

  return direction
}

export function timeToString (time = Date.now()) {
  const date = new Date(time);
  const todayUTC = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()))
  return todayUTC.toISOString().split('T')[0]
}

export function getMetricMetaInfo(metric){
  // Get the things that we need to know about each data type
  const info = {
    run: {
      displayName: 'Run',
      max: 50,
      unit: 'miles',
      step: 1,
      type: 'steppers',
      getIcon(){
        return(
            <View style={[styles.iconContainer, {backgroundColor: red}]}>
              <MaterialIcons name = 'directions-run' color = {white} size = {35}/>
            </View>
        )
      }
    },
    bike: {
      displayName: 'Bike',
      max: 50,
      unit: 'miles',
      step: 1,
      type: 'steppers',
      getIcon(){
        return(
            <View style={[styles.iconContainer, {backgroundColor: orange}]}>
              <MaterialCommunityIcons name = 'bike' color = {white} size = {35}/>
            </View>
        )
      }
    },
    swim: {
      displayName: 'Swim',
      max: 9900,
      unit: 'meters',
      step: 100,
      type: 'steppers',
      getIcon(){
        return(
            <View style={[styles.iconContainer, {backgroundColor: blue}]}>
              <MaterialCommunityIcons name = 'swim' color = {white} size = {35}/>
            </View>
        )
      }
    },
    sleep: {
      displayName: 'Sleep',
      max: 24,
      unit: 'hours',
      step: 100,
      type: 'slider',
      getIcon(){
        return(
            <View style={[styles.iconContainer, {backgroundColor: lightPurp}]}>
              <FontAwesome name = 'bed' color = {white} size = {35}/>
            </View>
        )
      }
    },
    eat: {
      displayName: 'Eat',
      max: 10,
      unit: 'rating',
      step: 100,
      type: 'slider',
      getIcon(){
        return(
            <View style={[styles.iconContainer, {backgroundColor: pink}]}>
              <MaterialCommunityIcons name = 'food' color = {white} size = {35}/>
            </View>
        )
      }
    },
  };

  // If metric is undefined then return the entire info object
  if (typeof metric === 'undefined'){
    return info;
  }
  else {
    return info[metric];
  }
}
/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getDailyReminderValue(){
  const possibleMessages = [
      "Don't get to log your data today!",
      "Remember to log your data and keep at your goals!",
      "You haven't logged your data today yet. Don't give up you can do it!",
      "Reminder to log your data!"
  ];
  const randomMessage = "👋 " +  + possibleMessages[getRandomInt(0, possibleMessages.length-1)];
  return {
    today: randomMessage,
  }
}