import React from 'react';
import { View, Text, StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  small: {
    fontSize: 9,
  },
});
export default class RenderLog extends React.Component {
  render(){
    const { description, inputObject } = this.props;
    const stringObject = JSON.stringify(inputObject);
  return(
      <View>

        <Text style={styles.small}>
        ------------------------------------
        </Text>
        <Text style={styles.small}>
          {description + ":"}
        </Text>
        <Text style={styles.small}>
        {stringObject}
        <Text>{`\n`}</Text>
        ------------------------------------
        </Text>
      </View>

      )
  }



}

